(define-module
  (zerolib)
  #:export (
            let1
            write-line
            write-all-lines
            read-all-lines
            current-source-file))

(use-modules
  (ice-9 match)
  (scheme base)
  (srfi srfi-26))

(define-syntax-rule
  (let1 (name value) expr expr* ...)
  (let ((name value)) expr expr* ...))

(define*
  (write-line text #:optional (port (current-output-port)))
  (display text port)
  (newline port))

(define*
  (write-all-lines lst #:optional (port (current-output-port)))
  (for-each
    (lambda (line)
      (write-line line port))
    lst))

(define*
  (read-all-lines #:optional (port (current-input-port)))
  "read input port to list of strings"
  (let loop ((result '()))
    (let1 (line (read-line port))
          (if (eof-object?  line)
            (reverse result)
            (loop (cons line result))))))

(define-public (swap func)
  (lambda (foo bar) (func bar foo)))

(define-public (true-list . args)
         (filter identity args))
(define-public (split-lines str)
  (string-split str #\newline))

(define-public (last lst)
  (car (reverse lst)))

(define-public s+ string-append)
(define-public (string-pascal-case str)
  (apply string-append (map string-capitalize (string-split str #\-))))
(define-public (string-camel-case str)
  (apply string-append (match (string-split str #\-) ((a . b) (cons a (map string-capitalize b))))))

(define-syntax current-source-file
  (lambda (s)
    "Return the name of the current file, or #f if it could not
    be determined."
    (syntax-case s ()
                 ((_)
                  (match (assq 'filename (or (syntax-source s) '()))
                         (('filename . (? string? file-name))
                          file-name)
                         ((or ('filename . #f) #f)
                          ;; raising an error would upset Geiser users
                          #f))))))
